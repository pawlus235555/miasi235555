package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

public class MyTreeParser extends TreeParser {

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected void declareVar(String name) {
		globalSymbols.newSymbol(name);
	}
	
	protected Integer getVar(String name) {
		return globalSymbol(name);
	}
	
	protected Integer setVar(String name) {
		globalSymbols.setSymbols(name, value);
		return value;
	}
	
	protected Integer add(Integer a, Integer b) {
		return a + b;
	}
	
	protected Integer sub(Integer a, Integer b) {
		return a - b;
	}
	
	protected Integer mul(Integer a, Integer b) {
		return a * b;
	}
	
	protected Integer div(Integer a, Integer b) {
		return a / b;
	}
	
}
